<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use Hash;

class UserController extends Controller
{
    //
    public function __construct(User $user) {
        $this->user = $user;
    }

    public function index() {
        return $this->user->all();
    }

    public function show($id) {
        $data = $this->user->where('id', '=', $id)->first();
        $status = 204;
        if ($data)
            $status = 200;
        return response()->json($data, $status);
    }

    public function update(Request $request, $id) {
        $credentials = $request->only(['username', 'name', 'email', 'password', 'dob', 'phone']);

        $credentials = [
            'name' => $credentials['name'],
            'username' => $credentials['username'],
            'email' => $credentials['email'],
            'password' => Hash::make($credentials['password']),
            'dob' => $credentials['dob'],
            'phone' => $credentials['phone']
        ];

        $user = $this->user->where('id', '=', $id);

        try {
            $user->update($credentials);
        } catch (Exception $e) {
            return response()->json(['error' => 'Same data already exists'], 409);
        }

        $user = $this->user->where('id', '=', $id)->first();
        return response()->json($user, 200);
    }

    public function byEmail(Request $request) {
        $user = $this->user->where('email', '=', $request->email)->first();
        if ($user)
            return response()->json($user, 200);
        else
            return response()->json($user, 204);
    }

    public function search(Request $request) {
        $user = $this->user->where('name', 'like', '%'.$request->name.'%')->get();
        if ($user)
            return response()->json($user, 200);
        else
            return response()->json($user, 204);
    }
}
