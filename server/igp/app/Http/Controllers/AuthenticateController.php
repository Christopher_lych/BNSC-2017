<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \Input as Input;

use App\Http\Requests;

use App\User;

use JWTAuth;
use Hash;

class AuthenticateController extends Controller
{
    //
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Invalid credentials'], 401);
        }

        $user = $this->user->where('email', '=', $credentials['email'])->first();

        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        $credentials = $request->only(['username', 'name', 'email', 'password', 'dob', 'phone']);

        $credentials = [
            'name' => $credentials['name'],
            'username' => $credentials['username'],
            'email' => $credentials['email'],
            'password' => Hash::make($credentials['password']),
            'dob' => $credentials['dob'],
            'phone' => $credentials['phone']
        ];

        try {
            $user = $this->user->create($credentials);
        } catch (Exception $e) {
            return response()->json(['error' => 'User already exists'], 409);
        }

//        $file = Input::file('image');
//        $file->move('uploads', $file->getClientOriginalName());

//        $credentials = [
//            'name' => $credentials['name'],
//            'username' => $credentials['username'],
//            'email' => $credentials['email'],
//            'password' => Hash::make($credentials['password']),
//            'dob' => $credentials['dob'],
//            'phone' => $credentials['phone'],
//            'url' => $file->getClientOriginalName()
//        ];

//        $user = $this->user->update($credentials);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('token'));
    }
}
