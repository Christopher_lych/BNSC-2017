<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class scoreModel extends Model
{
    //
    public $timestamps=false;
    protected $table="score";
    protected $fillable=["user_id", "game_id", "value"];
    protected $guarded=[];

    public function user() {
        return $this->belongsTo('App\Model\userModel', 'user_id');
    }

    public function game() {
        return $this->belongsTo('App\Model\gameModel', 'game_id');
    }
}
