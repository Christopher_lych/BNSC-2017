<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class gameModel extends Model
{
    //
    public $timestamps=false;
    protected $table="game";
    protected $fillable=["name"];
    protected $guarded=[];
}
