class Tetrominoe{

    constructor(num) {
        this.tetro = [];
        this.color = "";
        this.r = 0;
        this.c = (300/2/30);
        this.nextTetro = 0;
        this.tetroType = num;
        this.rotation = 0;
        for(let i=0; i<4; i++) {
            this.tetro[i] = [];
            for(let j=0; j<4; j++) {
                this.tetro[i][j] = 0;
            }
        }
        this.chooseTetro();
    }

    chooseTetro() {
        this.nextTetro = Math.floor(Math.random()*7);
        switch(this.tetroType) {
            case 0:
                // BOX
                this.tetro[1][1] = 1;
                this.tetro[1][2] = 1;
                this.tetro[2][1] = 1;
                this.tetro[2][2] = 1;
                this.color = "#478aff";
                break;

            case 1:
                // letter i
                this.tetro[0][1] = 1;
                this.tetro[1][1] = 1;
                this.tetro[2][1] = 1;
                this.tetro[3][1] = 1;
                this.color = "#ff4c4c";
                break;

            case 2:
                // Letter L
                this.tetro[0][1] = 1;
                this.tetro[1][1] = 1;
                this.tetro[2][0] = 1;
                this.tetro[2][1] = 1;
                this.color = "#ffcd2b";
                break;

            case 3:
                // Letter J
                this.tetro[0][1] = 1;
                this.tetro[1][1] = 1;
                this.tetro[2][1] = 1;
                this.tetro[2][2] = 1;
                this.color = "#75ea4b";
                break;

            case 4:
                // letter S
                this.tetro[1][1] = 1;
                this.tetro[1][2] = 1;
                this.tetro[2][0] = 1;
                this.tetro[2][1] = 1;
                this.color =  "#ff5ec4";
                break;

            case 5:
                // letter T
                this.tetro[1][0] = 1;
                this.tetro[1][1] = 1;
                this.tetro[1][2] = 1;
                this.tetro[2][1] = 1;
                this.color = "#d3d02c";
                break;

            case 6:
                // letter Z
                this.tetro[1][0] = 1;
                this.tetro[1][1] = 1;
                this.tetro[2][1] = 1;
                this.tetro[2][2] = 1;
                this.color = "#47ceff";
                break;
        }
    }

    // draw the "next tetrominoe"
    drawNext(ctx) {
        ctx.font = "18px Segoe UI";
        ctx.fillStyle = "#666";
        ctx.fillText("Next: ", 11*30, 4*30);
        ctx.beginPath();
        ctx.rect(11*30, 5*30, 120, 120);
        ctx.strokeStyle = "#000000";
        ctx.stroke();
        ctx.closePath();

        for(let i=0; i<4; i++) {
            for(let j=0; j<4; j++) {
                // if part that is filled is not out of bounds, draw it
                if (this.tetro[i][j]) {
                    if (this.r+i >= 0) {
                        ctx.beginPath();
                        ctx.rect(11*30+(j*30), 5*30+(i*30), 30, 30);
                        ctx.fillStyle = "#000000";
                        ctx.fill();
                        ctx.closePath();

                        ctx.beginPath();
                        ctx.rect(11*30+(j*30)+2, 5*30+(i*30)+2, 26, 26);
                        ctx.fillStyle = this.color;
                        ctx.fill();
                        ctx.closePath();
                    }
                }
            }
        }
    }

    // draw the tetrominoe in the area
    draw(ctx) {
        /*

        this commented block is used to draw the 4x4 tetrominoe container

        console.log("drawing");
        ctx.beginPath();
        ctx.rect(this.c*30, this.r*30, 80, 80);
        ctx.strokeStyle = "#000000";
        ctx.stroke();
        ctx.closePath();if (r<0)

        */

        if (this.r<0) {
            for(let i=0; i<4; i++) {
                for(let j=0; j<4; j++) {
                    // if part that is filled is not out of bounds, draw it
                    if (this.tetro[i][j]) {
                        if (this.r+i >= 0) {
                            ctx.beginPath();
                            ctx.rect(this.c*30+(j*30), this.r*30+(i*30), 30, 30);
                            ctx.fillStyle = "#000000";
                            ctx.fill();
                            ctx.closePath();

                            ctx.beginPath();
                            ctx.rect(this.c*30+(j*30)+2, this.r*30+(i*30)+2, 26, 26);
                            ctx.fillStyle = this.color;
                            ctx.fill();
                            ctx.closePath();
                        }
                    }
                }
            }
        }
        else {
            for(let i=0; i<4; i++) {
                for(let j=0; j<4; j++) {
                    if (this.tetro[i][j]) {
                        ctx.beginPath();
                        ctx.rect(this.c*30+(j*30), this.r*30+(i*30), 30, 30);
                        ctx.fillStyle = "#000000";
                        ctx.fill();
                        ctx.closePath();

                        ctx.beginPath();
                        ctx.rect(this.c*30+(j*30)+2, this.r*30+(i*30)+2, 26, 26);
                        ctx.fillStyle = this.color;
                        ctx.fill();
                        ctx.closePath();
                    }
                }
            }
        }
    }

    rotate() {
        if (this.tetroType !== 0) {
            let temp = [];
            let i;
            let j;

            for(i=0; i<4; i++) {
                temp[i] = [];
                for(j=0; j<4; j++) {
                    temp[i][j] = 0;
                }
            }

            if (this.tetroType === 2 || this.tetroType === 3 || this.tetroType === 5) {
                for(i=0; i<3; i++) {
                    let k = 2;
                    for(j=0; j<3; j++) {
                        temp[i][j] = this.tetro[k-j][i];
                    }
                }
            }
            else if (this.tetroType === 1 || this.tetroType === 4 || this.tetroType === 6) {
                if (this.rotation === 0) {
                    for(i=0; i<4; i++) {
                        let k = 3;
                        for(j=0; j<4; j++) {
                            temp[i][j] = this.tetro[k - j][i];
                        }
                    }
                }
                else {
                    for(i=0; i<4; i++) {
                        let k = 3;
                        for(j=0; j<4; j++) {
                            temp[i][j] = this.tetro[j][k-i];
                        }
                    }
                }
            }

            // applies rotation to matrix
            for(i=0; i<4; i++) {
                for(j=0; j<4; j++) {
                    this.tetro[i][j] = temp[i][j];
                }
            }
        }
    }

}