document.addEventListener("keydown", function(e){
    switch(e.keyCode) {
        case 37:
            game.actions.push(0);
            break;
        case 39:
            game.actions.push(1);
            break;
        case 38:
            game.actions.push(2);
            break;
        case 40:
            game.actions.push(3);
            break;
    }
});