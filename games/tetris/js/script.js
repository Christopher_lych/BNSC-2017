let canvas = document.getElementById("myCanvas");
let ctx = canvas.getContext("2d");

let r = 0;  // represented in block coordinate
let c = 0;  // represented in block coordinate

// 1 block is 20px X 20px

// key map
// 0 -> left
// 1 -> right
// 2 -> up
// 3 -> down

let left = 0;
let right = 1;
let up = 2;
let down = 3;

let last = now = new Date().getTime();
let step = 500;
let dt = 0;
let actions = [];
let field = [];
let tetrominoes =[];
let currTetro;
let curr;

let color = [];
color[0] = "#478aff";
color[1] = "#ff4c4c";
color[2] = "#ffcd2b";
color[3] = "#75ea4b";
color[4] = "#ff5ec4";
color[5] = "#d3d02c";
color[6] = "#47ceff";

// event listener
document.addEventListener("keydown", keyDownHandler);

// generate array of tetrominoes
function generateTetro() {

    // fill with 0
    for(i=0; i<7; i++) {
        tetrominoes[i] = [];
        for(j=0; j<4; j++) {
            tetrominoes[i][j] = [];
            for(k=0; k<4; k++) {
                tetrominoes[i][j][k] = 0;
            }
        }
    }

    // box  ->  tetrominoes[0]
    // 0 0 0 0
    // 0 1 1 0
    // 0 1 1 0
    // 0 0 0 0
    tetrominoes[0][1][1] = 1;
    tetrominoes[0][1][2] = 1;
    tetrominoes[0][2][1] = 1;
    tetrominoes[0][2][2] = 1;

    // letter "I"   ->  tetrominoes[1]
    // 0 1 0 0
    // 0 1 0 0
    // 0 1 0 0
    // 0 1 0 0
    tetrominoes[1][0][1] = 1;
    tetrominoes[1][1][1] = 1;
    tetrominoes[1][2][1] = 1;
    tetrominoes[1][3][1] = 1;

    // letter "J"   ->  tetrominoes[2]
    // 0 1 0 0
    // 0 1 0 0
    // 1 1 0 0
    // 0 0 0 0
    tetrominoes[2][0][1] = 1;
    tetrominoes[2][1][1] = 1;
    tetrominoes[2][2][0] = 1;
    tetrominoes[2][2][1] = 1;

    // letter "L"   ->  tetrominoes[3]
    // 0 1 0 0
    // 0 1 0 0
    // 0 1 1 0
    // 0 0 0 0
    tetrominoes[3][0][1] = 1;
    tetrominoes[3][1][1] = 1;
    tetrominoes[3][2][1] = 1;
    tetrominoes[3][2][2] = 1;

    // letter "S"   ->  tetrominoes[4]
    // 0 0 0 0
    // 0 1 1 0
    // 1 1 0 0
    // 0 0 0 0
    tetrominoes[4][1][1] = 1;
    tetrominoes[4][1][2] = 1;
    tetrominoes[4][2][0] = 1;
    tetrominoes[4][2][1] = 1;

    // letter "T"   ->  tetrominoes[5]
    // 0 0 0 0
    // 1 1 1 0
    // 0 1 0 0
    // 0 0 0 0
    tetrominoes[5][1][0] = 1;
    tetrominoes[5][1][1] = 1;
    tetrominoes[5][1][2] = 1;
    tetrominoes[5][2][1] = 1;

    // letter "Z"   ->  tetrominoes[6]
    // 0 0 0 0
    // 1 1 0 0
    // 0 1 1 0
    // 0 0 0 0
    tetrominoes[6][1][0] = 1;
    tetrominoes[6][1][1] = 1;
    tetrominoes[6][2][1] = 1;
    tetrominoes[6][2][2] = 1;

}

// empty the screen
function empty() {

    // fill the field with zeroes ( unoccupied )
    for(i=0; i<30; i++){
        field[i] = [];
        for(j=0; j<24; j++) {
            field[i][j] = {filled: 0, color: "#ffffff"};
        }
    }

}

// key handler
function keyDownHandler(e) {
    switch(e.keyCode) {
        case 37:
            actions.push(0);
            break;
        case 39:
            actions.push(1);
            break;
        case 38:
            actions.push(2);
            break;
        case 40:
            actions.push(3);
            break;
    }
}

// randomize and choose current tetrominoe and its color
function chooseTetro() {
    let rand = Math.floor(Math.random()*7);
    curr = rand;
    currTetro = tetrominoes[rand];
}

function drawTetro() {
    for(let i=0; i<4; i++) {
        for (let j = 0; j < 4; j++) {
            if (currTetro[i][j])
                ctx.clearRect(c*20+(j*20), r*20+(i*20), 20, 20);
        }
    }

    for(let i=0; i<4; i++) {
        for(let j=0; j<4; j++) {
            if (currTetro[i][j]) {
                ctx.beginPath();
                ctx.rect(c*20+(j*20), r*20+(i*20), 20, 20);
                ctx.fillStyle = "#000000";
                ctx.fill();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(c*20+(j*20)+2, r*20+(i*20)+2, 16, 16);
                ctx.fillStyle = color[curr];
                ctx.fill();
                ctx.closePath();
            }
        }
    }
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    for(let i=0; i<30; i++) {
        for(let j=0; j<24; j++) {
            if (field[i][j].filled) {
                ctx.beginPath();
                ctx.rect(j*20, i*20, 20, 20);
                ctx.fillStyle = "#000000";
                ctx.fill();
                ctx.closePath();

                ctx.beginPath();
                ctx.rect(j*20, i*20, 16, 16);
                ctx.fillStyle = field[i][j].color;
                ctx.fill();
                ctx.closePath();
            }
        }
    }
}

// move left or right
function move(dir) {
    if (dir === left) {

    }
    else {

    }
}

function drop() {
    let placed = false;

    console.log(r);

        // iterate from bottom left corner
    for(i=3; i>=0; i--) {
        for(j=0; j<4; j++) {
            // if current block is a part of tetrominoe
            if (currTetro[i][j]) {
                // if the tetrominoe reaches the bottom of the canvas
                if (r+i+1 === 30) {
                    for(x=r; x<(r+4); x++) {
                        for(y=c; y<(c+4); y++) {
                            if (x < 30)
                                if (!field[x][y].filled) {
                                    field[x][y].filled = currTetro[x-r][y-c];
                                    field[x][y].color = color[curr];
                                }
                        }
                    }
                    placed = true;
                    break;
                }

                // if the block directly below the current tetrominoe is occupied, then place the tetrominoe
                if (field[r+i+1][c].filled) {
                    for(x=r; x<(r+4); x++) {
                        for(y=c; y<(c+4); y++) {
                            if (x < 30)
                                if (!field[x][y].filled) {
                                    field[x][y].filled = currTetro[x-r][y-c];
                                    field[x][y].color = color[curr];
                                }
                        }
                    }
                    placed = true;
                    break;
                }
            }
        }
        if (placed)
            break;
    }
    // if it's not placed yet, just move the row one block below
    if (!placed) {
        r++;
    }
    else {
        chooseTetro();
        r = 0;
    }
}

// handle input ->  go to move / drop / rotate
function handle(act) {
    switch(act) {
        case 0:
            move(left);
            break;
        case 1:
            move(right);
            break;
        case 2:
            // rotate();
            break;
        case 3:
            drop();
            break;
    }
}

// drop if its' time to drop or move/rotate/drop based on input
function update(idt) {
    handle(actions.shift());
    draw();
    drawTetro();
    dt += idt;
    if (dt > step) {
        dt = dt-step;
        drop();
        draw();
        drawTetro();
    }
}

// "main function"-like functionality
function frame() {
    now = new Date().getTime();
    update(now-last);
    draw();
    drawTetro();
    last = now;
    requestAnimationFrame(frame, canvas);
}

generateTetro();
empty();
chooseTetro();
frame();
