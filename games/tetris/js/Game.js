class Game {

    constructor(canvas) {
        this.canvas = canvas;
        this.ctx = canvas.getContext("2d");
        this.field = [];
        this.actions = [];
        this.step = 500;
        this.dt = 0;
        this.left = 0;
        this.right = 1;
        this.up = 2;
        this.down = 3;
        this.score = 0;
        this.playing = true;
        this.last = new Date().getTime();
        this.now = new Date().getTime();
        this.tetrominoe;
        this.nextTetrominoe;
    }

    init() {
        this.tetrominoe = new Tetrominoe(Math.floor(Math.random()*7));
        this.nextTetrominoe = new Tetrominoe(this.tetrominoe.nextTetro);
        this.empty();
        this.last = new Date().getTime();
        this.now = new Date().getTime();
        this.score = 0;
        this.frame();
    }

    // to empty the field
    empty() {
        for(let i=0; i<18; i++) {
            this.field[i] = [];
            for(let j=0; j<10; j++) {
                this.field[i][j] = {filled: 0, color: ""};
            }
        }
    }

    place(r, c) {
        for(let x=r; x<(r+4); x++) {
            for(let y=c; y<(c+4); y++) {
                if (x > 17)
                    break;
                if (y<0 || y>9)
                    continue;
                if (!this.field[x][y].filled) {
                    this.field[x][y].filled = this.tetrominoe.tetro[x-r][y-c];
                    // console.log("filled["+x+"]["+y+"] = "+this.field[x][y].filled);
                    this.field[x][y].color = this.tetrominoe.color;
                }
            }
        }
    }

    rotate() {
        let temp = [];
        let r = this.tetrominoe.r;
        let c = this.tetrominoe.c;
        let type = this.tetrominoe.tetroType;
        let valid = true;

        if (type === 2 || type === 3 || type === 5) {
            for(let i=0; i<3; i++) {
                temp[i] = [];
                let k = 2;
                for(let j=0; j<3; j++) {
                    temp[i][j] = this.tetrominoe.tetro[k-j][i];
                    if (temp[i][j]) {
                        if ((r+i < 0) || (r+i > 17) || (c+j < 0) || (c+j > 9)) {
                            valid = false;
                            break;
                        }
                        if (this.field[r+i][c+j].filled) {
                            valid = false;
                            break;
                        }
                    }
                }
                if (!valid)
                    break;
            }
        }
        else if (type === 1 || type === 4 || type === 6) {
            if (this.tetrominoe.rotation === 0) {
                for(let i=0; i<4; i++) {
                    temp[i] = [];
                    let k = 3;
                    for(let j=0; j<4; j++) {
                        temp[i][j] = this.tetrominoe.tetro[k-j][i];
                        if (temp[i][j]) {
                            if ((r+i < 0) || (r+i > 17) || (c+j < 0) || (c+j > 9)) {
                                valid = false;
                                break;
                            }
                            if (this.field[r+i][c+j].filled) {
                                valid = false;
                                break;
                            }
                        }
                    }
                    if (!valid)
                        break;
                }
            }
            else {
                for(let i=0; i<4; i++) {
                    temp[i] = [];
                    let k = 3;
                    for(let j=0; j<4; j++) {
                        temp[i][j] = this.tetrominoe.tetro[j][k-i];
                        if (temp[i][j]) {
                            if ((r+i < 0) || (r+i > 17) || (c+j < 0) || (c+j > 9)) {
                                valid = false;
                                break;
                            }
                            if (this.field[r+i][c+j].filled) {
                                valid = false;
                                break;
                            }
                        }
                    }
                    if (!valid)
                        break;
                }
            }
        }

        if (valid) {
            this.tetrominoe.rotate();
            this.tetrominoe.rotation = (this.tetrominoe.rotation + 1) % 2;
        }

        this.drawAll();
        this.tetrominoe.draw(this.ctx);
        this.nextTetrominoe.drawNext(this.ctx);
    }

    move(dir) {

        let r = this.tetrominoe.r;
        let c = this.tetrominoe.c;

        let flag = false;   // flag true means it is not allowed to move left/right

        if (dir === this.left) {
            for(let i=0; i<4; i++) {
                for(let j=0; j<4; j++) {
                    if (this.tetrominoe.tetro[j][i]) {
                        if (c+i-1 < 0) {
                            flag = true;
                            break;
                        }
                        if (this.field[r+j][c+i-1].filled) {
                            console.log(j+" "+(c+i-1)+" is filled");
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag)
                    break;
            }
            if (!flag) {
                this.tetrominoe.c--;
            }
        }
        else {
            for(let i=3; i>=0; i--) {
                for(let j=0; j<4; j++) {
                    if (this.tetrominoe.tetro[j][i]) {
                        if (c+i+1 > 9) {
                            flag = true;
                            break;
                        }
                        if (this.field[r+j][c+i+1].filled) {
                            console.log(j+" "+(c+i+1)+" is filled");
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag)
                    break;
            }
            if (!flag) {
                this.tetrominoe.c++;
            }
        }

        this.drawAll();
        this.tetrominoe.draw(this.ctx);
        this.nextTetrominoe.drawNext(this.ctx);
    }

    drop() {
        let valid = true;
        let placed = false;
        let r = this.tetrominoe.r;
        let c = this.tetrominoe.c;

        this.drawAll();
        for(let i=3; i>=0; i--) {
            for(let j=0; j<4; j++) {
                // if current block of 4x4 is occupied
                if (this.tetrominoe.tetro[i][j]) {
                    // if the tetrominoe reaches the bottom of the canvas
                    if (r+i+1 > 17) {
                        this.place(r,c);
                        placed = true;
                        break;
                    }

                    // if the block directly below the current tetrominoe is occupied, then place the tetrominoe
                    if (this.field[r+i+1][c+j].filled) {
                        this.place(r,c);
                        placed = true;
                        break;
                    }
                }
            }
            if (placed)
                break;
        }

        // if it is not placed yet, just drop the tetrominoe one row below
        if (!placed) {
            this.tetrominoe.r = this.tetrominoe.r + 1;
            this.nextTetrominoe.drawNext(this.ctx);
            this.tetrominoe.draw(this.ctx);
        }
        // if it is placed, check if user clears a line or loses, and create new tetrominoe
        else {
            this.tetrominoe.draw(this.ctx);
            this.checkWin();
            this.tetrominoe = this.nextTetrominoe;
            this.nextTetrominoe = new Tetrominoe(this.tetrominoe.nextTetro);
            this.nextTetrominoe.drawNext(this.ctx);
            this.checkLose();
        }
    }

    // to clear a line and drop blocks above cleared line
    clearLine(line) {
        let i,j;

        for(i=0; i<10; i++) {
            this.field[line][i].filled = 0;
        }

        for(i=line-1; i>=0; i--) {
            let counter = 0;
            for(j=0; j<10; j++) {
                if (this.field[i][j].filled) {
                    counter++;
                    this.field[i+1][j].filled = 1;
                    this.field[i+1][j].color = this.field[i][j].color;
                    this.field[i][j].filled = 0;
                }
            }
            if (counter === 0)
                break;
        }

        this.drawAll();
        this.drawScore();
    }

    // check for complete line(s)
    checkWin() {
        let i = 17;
        while(i>=0) {
            let counter = 0;
            for(let j=0; j<10; j++) {
                if (this.field[i][j].filled) {
                    counter++;
                }
            }
            if (counter === 0) {
                break;
            }
            if (counter === 10) {
                this.clearLine(i);
                this.score++;
                if (this.score % 5 === 0)
                    this.step -= 20;
            }
            else {
                i--;
            }
        }
    }

    // check if middle line is clogged
    checkLose() {
        let r = this.tetrominoe.r;
        let c = this.tetrominoe.c;
        let valid = true;

        for(let i=3; i>=0; i--) {
            for(let j=0; j<4; j++) {
                if (this.field[r+i][c+j].filled) {
                    valid = false;
                    break;
                }
            }
            if (!valid)
                break;
        }

        if (!valid) {
            started = false
            this.clear();
            // draw the score and lose message
            this.ctx.font = "18px Segoe UI";
            this.ctx.fillStyle = "#666";
            this.ctx.fillText("GAME OVER!", 6*30, 6*30);
            this.ctx.fillText("Your Final Score: "+this.score, 5*30, 8*30);
            this.ctx.fillText("Press Space to Play Again", 4*30, 10*30);
        }
    }

    clear() {
        this.ctx.clearRect(0, 0, canvas.width, canvas.height);
    }

    drawScore() {
        this.ctx.font = "18px Segoe UI";
        this.ctx.fillStyle = "#666";
        this.ctx.fillText("Score: "+this.score, 11*30, 15*30);
    }

    drawAll() {
        this.clear();

        this.ctx.clearRect(0, 0, canvas.width, canvas.height);
        for(let i=0; i<18; i++) {
            for(let j=0; j<10; j++) {
                if (this.field[i][j].filled) {
                    // draw the border
                    this.ctx.beginPath();
                    this.ctx.rect(j*30, i*30, 30, 30);
                    this.ctx.fillStyle = "#000000";
                    this.ctx.fill();
                    this.ctx.closePath();

                    // draw the inner part
                    this.ctx.beginPath();
                    this.ctx.rect(j*30+2, i*30+2, 26, 26);
                    this.ctx.fillStyle = this.field[i][j].color;
                    this.ctx.fill();
                    this.ctx.closePath();
                }
                else {
                    this.ctx.beginPath();
                    this.ctx.rect(j*30, i*30, 30, 30);
                    this.ctx.strokeStyle = "#ccc";
                    this.ctx.stroke();
                    this.ctx.closePath();
                }
            }
        }
        this.drawScore();
    }

    handle(act) {
        switch(act) {
            case 0:
                this.move(this.left);
                break;
            case 1:
                this.move(this.right);
                break;
            case 2:
                this.rotate();
                break;
            case 3:
                this.drop();
                break;
        }
    }

    update(idt) {
        this.handle(this.actions.shift());
        this.dt += idt;
        if (this.dt > this.step) {
            this.dt -= this.step;
            this.drop();
        }
    }

    frame() {
        if (started) {
            this.now = new Date().getTime();
            this.update(this.now - this.last);
            this.last = this.now;

            setTimeout(() => {
                this.frame();
            }, 20);
        }
    }
}