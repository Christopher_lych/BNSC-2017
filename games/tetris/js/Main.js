let canvas = document.getElementById("myCanvas");
let game = new Game(canvas);
let started = false;
canvas.getContext("2d").font = "20px Segoe UI";
canvas.getContext("2d").fillStyle = "#444";
canvas.getContext("2d").fillText("Press Space To Start", 5*30, 8*30);

window.onkeydown = function(e) {
    if (!started){
        if (e.keyCode == 32){
            started = true;
            game.init();
        }
    }
};