class Game {

    constructor() {
        this.lives;                 // remaining lives
        this.invisible;             // invisibility from obstacles status
        this.score;                 // user score
        this.gameSpeed;             // speed of scrolling
        this.now;                   // current timestamp
        this.last;                  // previous timestamp
        this.coins = [];            // coins container
        this.backgrounds = [];      // backgrounds container
        this.obsUrl = [];           // url of obstacle img source
        this.obstacles = [];        // obstacles container
        resources.load(["./assets/images/dragon2.png",
            "./assets/images/spinning_coin_gold.png",
            "./assets/images/obstacle/obs_btm_A.png",
            "./assets/images/obstacle/obs_top_A.png",
            "./assets/images/obstacle/obs_btm_B.png",
            "./assets/images/obstacle/obs_top_B.png"
        ]);

        // map the url of imgs to array
        this.obsUrl[0] = "./assets/images/obstacle/obs_btm_A.png";
        this.obsUrl[1] = "./assets/images/obstacle/obs_top_A.png";
        this.obsUrl[2] = "./assets/images/obstacle/obs_btm_B.png";
        this.obsUrl[3] = "./assets/images/obstacle/obs_top_B.png";

        // load backgrounds
        let background = new Picture("./assets/images/background/bg_back.png", [0,0]);
        this.backgrounds[this.backgrounds.length] = background;
        background = new Picture("./assets/images/background/bg_front_ground.png", [0,0]);
        this.backgrounds[this.backgrounds.length] = background;
        background = new Picture("./assets/images/background/bg_middle.png", [0,0]);
        this.backgrounds[this.backgrounds.length] = background;
        background = new Picture("./assets/images/background/bg_superfront.png", [0,0]);
        this.backgrounds[this.backgrounds.length] = background;

    }

    init() {
        this.lives = 3;
        this.score = 0;
        this.invisible = false;
        this.gameSpeed = 0.4;
        this.last = new Date().getTime();

        this.dragon = {
                pos: [20, 125],
                sprite: new Sprite("./assets/images/dragon2.png", [0,0], [128,128], 0.04, [0,1,2,3,4,5,6,7,8,9,10,11])
            };

        // reset background position to 0
        for(let i=0; i<this.backgrounds.length; i++) {
            this.backgrounds[i].pos[0] = 0;
        }

        this.obstacles.length = 0;
        this.coins.length = 0;

        this.generateCoins();
        this.generateObstacle();
        this.increaseSpeed();
        this.main();
    }

    increaseSpeed() {
        if (started) {
            this.gameSpeed += 0.04;
            setTimeout(() => {
                this.increaseSpeed();
            }, 4000);
        }
    }

    // generates an obstacle every 5 seconds
    generateObstacle() {
        if (started) {
            // randomize which obstacle we will get
            let res = Math.floor(Math.random()*4);
            let obstacle;

            switch(res) {
                case 0:
                    obstacle = {
                        pos: [720, 270],
                        sprite: new Sprite(this.obsUrl[0], [0,0], [45,102], 0, [])
                    };
                    break;
                case 1:
                    obstacle = {
                        pos: [720, 30],
                        sprite: new Sprite(this.obsUrl[1], [0,0], [45,102], 0, [])
                    };
                    break;
                case 2:
                    obstacle = {
                        pos: [720, 279],
                        sprite: new Sprite(this.obsUrl[2], [0,0], [31,92], 0, [])
                    };
                    break;
                case 3:
                    obstacle = {
                        pos: [720, 30],
                        sprite: new Sprite(this.obsUrl[3], [0,0], [31,92], 0, [])
                    };
                    break;
            }
            this.obstacles.push(obstacle);

            setTimeout(() => {
                this.generateObstacle();
            }, 3000);
        }
    }

    // generates 3x3 box of coins every 5 seconds
    generateCoins() {
        if (started) {
            let res = Math.floor(Math.random()*100);
            for(let i=0; i<9; i++) {
                let coin = {
                    pos: [720+(Math.floor(i/3)*32), 100+res+(i%3)*32],
                    sprite: new Sprite("./assets/images/spinning_coin_gold.png", [0,0], [24,24], 0.025, [0,1,2,3,4,5,6,7])
                };
                this.coins.push(coin);
            }

            setTimeout(() => {
                this.generateCoins();
            }, 5000);
        }
    }

    collides(x, y, r, c, x2, y2, r2, c2) {
        return !(r <= x2 || x > r2 ||
                 c <= y2 || y > c2);
    }

    boxCollides(pos, size, pos2, size2) {
        return this.collides(pos[0], pos[1],
                        pos[0] + size[0], pos[1] + size[1],
                        pos2[0], pos2[1],
                        pos2[0] + size2[0], pos2[1] + size2[1]);
    }

    checkCollisions() {
        // iterator
        let i;

        // check for collisions with coins
        for(i=this.coins.length-1; i>=0; i--) {
            // check if collides with tail
            if (this.boxCollides([this.dragon.pos[0], this.dragon.pos[1]+70], [17,25], this.coins[i].pos, this.coins[i].sprite.size)) {
                this.score += 10;
                this.coins.splice(i,1);
            }
            // check if collides with body and wings
            else if (this.boxCollides([this.dragon.pos[0]+18, this.dragon.pos[1]+6], [56, 115], this.coins[i].pos, this.coins[i].sprite.size)) {
                this.score += 10;
                this.coins.splice(i,1);
            }
            // check if collides with head
            else if (this.boxCollides([this.dragon.pos[0]+75, this.dragon.pos[1]+46], [45, 38], this.coins[i].pos, this.coins[i].sprite.size)) {
                this.score += 10;
                this.coins.splice(i,1);
            }
        }

        // check for collisions with obstacles
        for(i=this.obstacles.length-1; i>=0; i--) {
            if (!this.invisible) {
                // if current obstacles is big top obstacle
                if (this.obstacles[i].sprite.url === "./assets/images/obstacle/obs_top_A.png") {
                    // tail
                    // check if lower part collides
                    if (this.boxCollides([this.dragon.pos[0], this.dragon.pos[1]+70], [17,25], [this.obstacles[i].pos[0], this.obstacles[i].pos[1]+40], [24,62])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                    // check if lower part collides
                    else if (this.boxCollides([this.dragon.pos[0], this.dragon.pos[1]+70], [17,25], [this.obstacles[i].pos[0]+26, this.obstacles[i].pos[1]], [19,33])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }

                    // body
                    // check if lower part collides
                    else if (this.boxCollides([this.dragon.pos[0]+18, this.dragon.pos[1]+6], [56, 115], [this.obstacles[i].pos[0], this.obstacles[i].pos[1]+40], [24,62])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                    // check if upper part collides
                    else if (this.boxCollides([this.dragon.pos[0]+18, this.dragon.pos[1]+6], [56, 115], [this.obstacles[i].pos[0]+26, this.obstacles[i].pos[1]], [19,33])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }

                    // head
                    // check if lower part collides
                    else if (this.boxCollides([this.dragon.pos[0]+75, this.dragon.pos[1]+46], [45, 38], [this.obstacles[i].pos[0], this.obstacles[i].pos[1]+40], [24,62])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                    // check if upper part collides
                    else if (this.boxCollides([this.dragon.pos[0]+75, this.dragon.pos[1]+46], [45, 38], [this.obstacles[i].pos[0]+26, this.obstacles[i].pos[1]], [19,33])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                }
                // if current obstacle is big bottom obstacle
                else if (this.obstacles[i].sprite.url === "./assets/images/obstacle/obs_btm_A.png") {
                    // tail
                    // check if upper part collides
                    if (this.boxCollides([this.dragon.pos[0], this.dragon.pos[1]+70], [17,25], this.obstacles[i].pos, [31,71])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                    // check if lower part collides
                    if (this.boxCollides([this.dragon.pos[0], this.dragon.pos[1]+70], [17,25], [this.obstacles[i].pos[0]+16, this.obstacles[i].pos[1]+72], [29,30])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }

                    //wings and body
                    // check if upper part collides
                    else if (this.boxCollides([this.dragon.pos[0]+18, this.dragon.pos[1]+6], [56, 115], this.obstacles[i].pos, [31,71])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                    // check if lower part collides
                    else if (this.boxCollides([this.dragon.pos[0]+18, this.dragon.pos[1]+6], [56, 115], [this.obstacles[i].pos[0]+16, this.obstacles[i].pos[1]+72], [29,30])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }

                    // head
                    // check if upper part collides
                    else if (this.boxCollides([this.dragon.pos[0]+75, this.dragon.pos[1]+46], [45, 38], this.obstacles[i].pos, [31,71])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                    // check if lower part collides
                    else if (this.boxCollides([this.dragon.pos[0]+75, this.dragon.pos[1]+46], [45, 38], [this.obstacles[i].pos[0]+16, this.obstacles[i].pos[1]+72], [29,30])) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                }
                else {
                    // check if collides with tail
                    if (this.boxCollides([this.dragon.pos[0], this.dragon.pos[1]+70], [17,25], this.obstacles[i].pos, this.obstacles[i].sprite.size)) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                    // check if collides with body and wings
                    else if (this.boxCollides([this.dragon.pos[0]+18, this.dragon.pos[1]+6], [56, 115], this.obstacles[i].pos, this.obstacles[i].sprite.size)) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                    // check if collides with head
                    else if (this.boxCollides([this.dragon.pos[0]+75, this.dragon.pos[1]+46], [45, 38], this.obstacles[i].pos, this.obstacles[i].sprite.size)) {
                        this.score--;
                        this.lives--;
                        this.invisible = true;
                        setTimeout(() => {
                            this.invisible = false;
                        }, 3000);
                    }
                }
            }
        }

        if (!this.lives)
            started = false;

    }

    main() {
        if (started) {
            this.update();
            this.render();
            setTimeout(() => {
                this.main();
            }, 20);
        }
        else {
            ctx.beginPath();
            ctx.rect(0,0,720,480);
            ctx.fillStyle = "rgba(220,220,220,0.85)";
            ctx.fillOpacity = 0.1;
            ctx.fill();
            ctx.closePath();

            ctx.font = "32px Segoe UI";
            ctx.fillStyle = "#444";
            ctx.fillText("Game Over!", 300, 225);
            ctx.fillText("Press Space to Play Again", 225, 280);
        }
    }

    updateEntities() {
        let i;      // iterator

        // update background
        for(i=0; i<this.backgrounds.length; i++) {
            this.backgrounds[i].pos[0] -= this.gameSpeed;
            if (this.backgrounds[i].pos[0] + 1600 <= 0) {
                this.backgrounds[i].pos[0] = 0;
            }
        }

        // update coins
        for(i=0; i<this.coins.length; i++) {
            this.coins[i].sprite.update(this.now-this.last);
            this.coins[i].pos[0] -= this.gameSpeed;
        }

        // update obstacles
        for(i=0; i<this.obstacles.length; i++) {
            this.obstacles[i].sprite.update(this.now-this.last);
            this.obstacles[i].pos[0] -= this.gameSpeed;
        }

        // update dragon
        this.dragon.sprite.update(this.now-this.last);


        /*      below are loops to check for objects that are out of bounds       */

        // coins
        for(i=this.coins.length-1; i>=0; i--) {
            if (this.coins[i].pos[0] + 24 <= 0)
                this.coins.splice(i, 1);
        }

        // obstacles
        for(i=this.obstacles.length-1; i>=0; i--) {
            if (this.obstacles[i].pos[0] + 50 <= 0)
                this.obstacles.splice(i, 1);
        }
    }

    update() {
        this.now = new Date().getTime();
        this.updateEntities();
        this.checkCollisions();
        this.last = this.now;
    }

    render() {
        // iterator variables
        let i, j;

        // clear the canvas and record current time
        ctx.clearRect(0, 0, 720, 480);

        // draw background (bushes, trunks, etc)
        for(i=0; i<this.backgrounds.length; i++) {
            ctx.drawImage(this.backgrounds[i].img, this.backgrounds[i].pos[0], this.backgrounds[i].pos[1]);
            ctx.drawImage(this.backgrounds[i].img, this.backgrounds[i].pos[0] + 1600, this.backgrounds[i].pos[1]);
        }

        // draw coins
        this.renderMultiple(this.coins);

        // draw obstacles
        this.renderMultiple(this.obstacles);

        // draw dragon
        this.renderSingle(this.dragon);

        if (this.invisible) {
            ctx.beginPath();
            ctx.rect(0,0,720,480);
            ctx.fillStyle = "rgba(160,0,0,0.2)";
            ctx.fillOpacity = 0.1;
            ctx.fill();
            ctx.closePath();
        }

        ctx.font = "20px Segoe UI";
        ctx.fillStyle = "#eee";
        ctx.fillText("Score: "+this.score, 20, 25);

        ctx.font = "20px Segoe UI";
        ctx.fillStyle = "#eee";
        ctx.fillText("Live(s): "+this.lives, 125, 25);
    }

    renderMultiple(list) {
        for(let i=0; i<list.length; i++) {
            this.renderSingle(list[i]);
        }
    }

    renderSingle(entity) {
        ctx.save();
        ctx.translate(entity.pos[0], entity.pos[1]);
        entity.sprite.render(ctx);
        ctx.restore();
    }
}