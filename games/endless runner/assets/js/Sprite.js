class Sprite {

    constructor(url, pos, size, speed, frames, dir, once) {
        this.url = url;                                         // url of sprite
        this.pos = pos;                                         // x and y coordinate of image in sprite
        this.size = size;                                       // size of sprite
        this.speed = typeof speed === 'number' ? speed : 0;     // speed in frames/sec
        this.frames = frames;                                   // array of frame indexes
        this.index = 0;
        this.dir = dir || 'horizontal';                         // direction of movement
        this.once = once;                                       //  run once or not
    }

    update(idt) {
        this.index += this.speed*idt;
    }

    render(ctx) {
        let frame;

        if (this.speed > 0) {
            let max = this.frames.length;
            let idx = Math.floor(this.index);
            frame = this.frames[idx % max];

            if (this.once && idx >= max) {
                this.done = true;
                return;
            }
        }
        else {
            frame = 0;
        }

        let x = this.pos[0];
        let y = this.pos[1];


        if (this.dir === 'vertical') {
            y += frame * this.size[1];
        }
        else {
            x += frame * this.size[0];
        }

        ctx.drawImage(resources.get(this.url),
                        x,y,
                        this.size[0], this.size[1],
                        0, 0,
                        this.size[0], this.size[1]);
    }
}