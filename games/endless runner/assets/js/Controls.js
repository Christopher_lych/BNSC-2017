document.addEventListener("keydown", function(e) {
    switch(e.keyCode) {
        case 37:
            game.dragon.pos[0] -= 10;
            break;
        case 38:
            game.dragon.pos[1] -= 15;
            break;
        case 39:
            game.dragon.pos[0] += 10;
            break;
        case 40:
            game.dragon.pos[1] += 15;
            break;
    }
});