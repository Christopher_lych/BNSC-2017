class Picture {

    constructor(url, pos) {
        this.img = new Image();
        this.img.src = url;
        this.pos = pos;
    }

    render(ctx) {
        this.pos[0] -= this.speed;
        ctx.drawImage(this.img, this.pos[0], this.pos[1]);
        ctx.drawImage(this.img, this.pos[0] + canvas.width, this.pos[1]);
        if (this.pos[0] + canvas.width <= 0) {
            this.pos[0] = 0;
        }
    }
}