let canvas = document.getElementById("myCanvas");
let ctx = canvas.getContext("2d");

let started = false;
let game = new Game();

canvas.getContext("2d").font = "32px Segoe UI";
canvas.getContext("2d").fillStyle = "#444";
canvas.getContext("2d").fillText("Press Space to Start", 230, 250);

window.onkeydown = function(e) {
    if (!started) {
        if (e.keyCode == 32) {
            started = true;
            game.init();
        }
    }
}