$(function() {

    // configuration
    let $content = $('#content');
    let $banner = $('#banner');
    let width = $content.width();
    let height = $banner.height();
    let animationSpeed = 1000;
    let pause = 3500;
    let currentSlide = 0;

    // cache DOM
    let $slider = $('#slider');
    let $slideContainer = $slider.find('.slides');
    let $slides = $slideContainer.find('.slide');
    let $bannerImg = $slides.find('.banner-img');
    let $next = $('.next');
    let $prev = $('.prev');

    // play button click
    // let playBtn = $('#play-btn');
    //
    // playBtn.on("click", function() {
    //     if (currentSlide === 0)
    //         alert("Runner");
    //     else
    //         alert("Tetris");
    // });

    // initial setting
    $slides.css("width", width+"px");
    $bannerImg.css("width", width+"px");
    $bannerImg.css("height", height+"px");

    // next and prev button listener
    $next.on("click", function() {
        stopSlider();
        currentSlide++;
        if (currentSlide === $slides.length) {
            currentSlide = 0;
            $slideContainer.animate({'margin-left': '0px'}, animationSpeed);
        }
        else {
            $slideContainer.animate({'margin-left': '-='+width+'px'}, animationSpeed);
        }
        startSlider();
    });

    $prev.on("click", function() {
        stopSlider();
        currentSlide--;
        if (currentSlide < 0) {
            currentSlide = $slides.length-1;
            $slideContainer.animate({'margin-left': '-='+currentSlide*width+'px'}, animationSpeed);
        }
        else {
            $slideContainer.animate({'margin-left': '+='+width+'px'}, animationSpeed);
        }
        startSlider();
    });

    // responsive size
    $(window).bind("resize", function() {
        width = $content.width();
        $slides.css("width", width+"px");
        $bannerImg.css("width", width+"px");
        $bannerImg.css("height", height+"px");
        $slideContainer.css("margin-left", "-"+currentSlide*width+"px");
    });

    let interval;
    // start sliding
    function startSlider() {
        interval = setInterval(function() {
            currentSlide++;
            if (currentSlide === $slides.length) {
                currentSlide = 0;
                $slideContainer.animate({'margin-left': '0px'}, animationSpeed);
            }
            else {
                $slideContainer.animate({'margin-left': '-='+width+'px'}, animationSpeed);
            }
        },pause);
    }

    // stop sliding
    function stopSlider() {
        clearInterval(interval);
    }

    $slider.on('mouseenter', stopSlider).on('mouseleave', startSlider);

    startSlider();
})