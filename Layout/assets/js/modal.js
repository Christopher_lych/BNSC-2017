// Get the modal, modal image, and caption
let modal = document.getElementById('myModal');
let modalImg = document.getElementById("modalImage");

// let $pict = $('.pict');
// let $img = $pict.find('img');

// $img.each(function() {
//    let that = $(this);
//    that.onclick = previewImage(this);
// });

function previewImage(img) {

    modal.style.display = "block";
    modalImg.src = img.src;
}

// Get the <span> element that closes the modal
let span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
};