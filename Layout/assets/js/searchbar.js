$(function() {

    // variables
    let $searchIcon = $('#search-icon');
    let $topbar = $('#topbar');
    let visible = false;

    $searchIcon.on("click", function() {
        if (!visible) {
            $topbar.show();
            visible = true;
        }
        else {
            $topbar.hide();
            visible = false;
        }
    })

});