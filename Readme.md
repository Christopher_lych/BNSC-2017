# Project Title

BNSC 2017 Project

## Deployment

Open folder \server\igp, and then open command prompt and type php artisan serve
Open index.html inside result\vue-webpack-gulp-package\dist

## Built With

* [Laravel](https://laravel.com/) - The php framework used
* [Vue JS](https://vuejs.org/) - Front End Framework used

## Authors

* **Christopher Lychealdo**